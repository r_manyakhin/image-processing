//
//  ImageTableViewCell.swift
//  ImageProcessing
//
//  Created by Роман Маняхин on 04.04.2018.
//  Copyright © 2018 Roman Manyakhin. All rights reserved.
//

import UIKit

class ImageTableViewCell: UITableViewCell {

  // MARK: - Outlets

  @IBOutlet weak var cellImage: UIImageView!

  // MARK: - Cell Standard Methods

  override func awakeFromNib() {
    super.awakeFromNib()
  }

  override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
  }

}
