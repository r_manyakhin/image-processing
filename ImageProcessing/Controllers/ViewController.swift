//
//  ViewController.swift
//  ImageProcessing
//
//  Created by Роман Маняхин on 03.04.2018.
//  Copyright © 2018 Roman Manyakhin. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

  // MARK: - Outlets

  @IBOutlet weak var selectImage: UIButton!

  @IBOutlet weak var rotateImage: UIButton!

  @IBOutlet weak var invertColorsImage: UIButton!

  @IBOutlet weak var mirrorImage: UIButton!

  @IBOutlet weak var imageView: UIImageView!

  @IBOutlet weak var table: UITableView!

  // MARK: - Variables

  var images: [UIImage] = []

  // MARK: - Controller Standard Methods

  override func viewDidLoad() {
    super.viewDidLoad()

    imageView.isHidden = true

    selectImage.orangeButton(NSLocalizedString("SelectImageButtonTitle", comment: ""))

    rotateImage.orangeButton(NSLocalizedString("RotateImageButtonTitle", comment: ""))

    invertColorsImage.orangeButton(NSLocalizedString("InvertColorsImageButtonTitle", comment: ""))

    mirrorImage.orangeButton(NSLocalizedString("MirrorImageButtonTitle", comment: ""))

    selectImage.addTarget(self, action: #selector(changeImage(_:)), for: .touchUpInside)

    rotateImage.addTarget(self, action: #selector(rotate(_:)), for: .touchUpInside)
    mirrorImage.addTarget(self, action: #selector(mirror(_:)), for: .touchUpInside)
    invertColorsImage.addTarget(self, action: #selector(invertColors(_:)), for: .touchUpInside)

    let tap = UITapGestureRecognizer(target: self, action: #selector(changeImageTap(_:)))
    imageView.addGestureRecognizer(tap)

    table.registerCellFromNib(ImageTableViewCell.self)

    table.dataSource = self
    table.delegate = self
  }

  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
  }

  // MARK: - Actions

  @objc func changeImage(_: UIButton) {
    let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)

    let photo = UIAlertAction(title: NSLocalizedString("ChangeImageAlertPhoto", comment: ""), style: .default) { _ in
      self.openImagePicker(.photoLibrary)
    }
    let camera = UIAlertAction(title: NSLocalizedString("ChangeImageAlertCamera", comment: ""), style: .default) { _ in
      self.openImagePicker(.camera)
    }
    let cancel = UIAlertAction(title: NSLocalizedString("ChangeImageAlertCancel", comment: ""), style: .cancel)

    alert.addAction(photo)
    alert.addAction(camera)
    alert.addAction(cancel)

    DispatchQueue.main.async {
      self.present(alert, animated: true)
    }
  }

  @objc func changeImageTap(_: UITapGestureRecognizer) {
    changeImage(selectImage)
  }

  @objc func rotate(_: UIButton) {
    if let newImage = imageView.image?.rotate() {
      images.append(newImage)

      table.reloadData()
    }
  }

  @objc func mirror(_: UIButton) {
    if let newImage = imageView.image?.flip() {
      images.append(newImage)

      table.reloadData()
    }
  }

  @objc func invertColors(_: UIButton) {
    if let newImage = imageView.image?.invertColors() {
      images.append(newImage)

      table.reloadData()
    }
  }

  // MARK: - Image Pickers Methods

  func openImagePicker(_ type: UIImagePickerControllerSourceType) {
    if UIImagePickerController.isSourceTypeAvailable(type) {
      let imagePickerController = UIImagePickerController()

      imagePickerController.sourceType = type
      imagePickerController.delegate = self

      DispatchQueue.main.async {
        self.present(imagePickerController, animated: true)
      }
    }
  }

}

extension ViewController: UINavigationControllerDelegate, UIImagePickerControllerDelegate {

  func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
    if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
      imageView.image = image

      imageView.isHidden = false

      selectImage.isHidden = true
    }

    picker.dismiss(animated: true)
  }

  @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
    var title = NSLocalizedString("SaveImageAlertTitle", comment: "")
    var message = NSLocalizedString("SaveImageAlertMessage", comment: "")

    if let error = error {
      title = NSLocalizedString("SaveImageAlertTitleError", comment: "")
      message = error.localizedDescription
    }

    let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)

    let ok = UIAlertAction(title: NSLocalizedString("SaveImageAlertOk", comment: ""), style: .cancel)

    alert.addAction(ok)

    DispatchQueue.main.async {
      self.present(alert, animated: true)
    }
  }

}

extension ViewController: UITableViewDelegate, UITableViewDataSource {

  func numberOfSections(in tableView: UITableView) -> Int {
    return images.count
  }

  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return 1
  }

  func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
    return 16.0
  }

  func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
    let view = UIView(frame: CGRect(origin: .zero, size: CGSize(width: UIScreen.main.bounds.width - 32.0, height: 16.0)))

    view.backgroundColor = UIColor.clear

    return view
  }

  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    let size = images[indexPath.section].size

    let height = ((UIScreen.main.bounds.width - 32.0) * size.height) / size.width

    return height
  }

  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueCell(ImageTableViewCell.self)

    cell.cellImage.image = images[indexPath.section]

    return cell
  }

  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    let index = indexPath.section

    let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)

    let save = UIAlertAction(title: NSLocalizedString("NewImageAlertSave", comment: ""), style: .default) { _ in
      let image = self.images[index]

      UIImageWriteToSavedPhotosAlbum(image, self, #selector(self.image(_:didFinishSavingWithError:contextInfo:)), nil)
    }
    let remove = UIAlertAction(title: NSLocalizedString("NewImageAlertRemove", comment: ""), style: .default) { _ in
      self.images.remove(at: index)

      self.table.reloadData()
    }
    let use = UIAlertAction(title: NSLocalizedString("NewImageAlertUse", comment: ""), style: .default) { _ in
      let image = self.images[index]

      self.imageView.image = image
    }
    let cancel = UIAlertAction(title: NSLocalizedString("NewImageAlertCancel", comment: ""), style: .cancel)

    alert.addAction(save)
    alert.addAction(remove)
    alert.addAction(use)
    alert.addAction(cancel)

    DispatchQueue.main.async {
      self.present(alert, animated: true)
    }
  }

}

