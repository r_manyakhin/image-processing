//
//  UIButton+Extension.swift
//  ImageProcessing
//
//  Created by Роман Маняхин on 03.04.2018.
//  Copyright © 2018 Roman Manyakhin. All rights reserved.
//

import UIKit

extension UIButton {

  func orangeButton(_ title: String) {
    self.backgroundColor = UIColor.orange

    self.layer.cornerRadius = 8.0

    self.setTitleColor(UIColor.white, for: .normal)

    self.titleLabel?.font = UIFont.openSansBold(14.0)

    self.setTitle(title, for: .normal)
  }

}
