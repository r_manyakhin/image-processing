//
//  UITableView+Extensions.swift
//  ImageProcessing
//
//  Created by Роман Маняхин on 04.04.2018.
//  Copyright © 2018 Roman Manyakhin. All rights reserved.
//

import UIKit

extension UITableView {

  public func registerCellFromNib<T>(_ type: T.Type = T.self, withIdentifier reuseIdentifier: String = String(describing: T.self)) where T: UITableViewCell {
    register(UINib(nibName: reuseIdentifier, bundle: nil), forCellReuseIdentifier: reuseIdentifier)
  }

  public func dequeueCell<T>(_ type: T.Type = T.self, withIdentifier reuseIdentifier: String = String(describing: T.self)) -> T where T: UITableViewCell {
    guard let cell = dequeueReusableCell(withIdentifier: reuseIdentifier) as? T else {
      fatalError("Unknown cell type (\(T.self)) for reuse identifier: \(reuseIdentifier)")
    }
    return cell
  }

}
