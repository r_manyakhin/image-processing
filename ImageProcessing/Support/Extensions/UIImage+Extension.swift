//
//  UIImage+Extension.swift
//  ImageProcessing
//
//  Created by Роман Маняхин on 03.04.2018.
//  Copyright © 2018 Roman Manyakhin. All rights reserved.
//

import UIKit

extension UIImage {

  public func flip() -> UIImage? {
    UIGraphicsBeginImageContext(size)

    if let bitmap = UIGraphicsGetCurrentContext(), let image = self.cgImage {
      bitmap.translateBy(x: size.width, y: size.height)

      bitmap.scaleBy(x: -self.scale, y: -self.scale)

      let rect = CGRect(origin: .zero, size: size)

      bitmap.draw(image, in: rect)

      let newImage = UIGraphicsGetImageFromCurrentImageContext()

      UIGraphicsEndImageContext()

      return newImage
    }

    return self
  }

  public func rotate() -> UIImage? {
    let degrees = CGFloat(Double.pi) / 2.0

    let rotatedSize = CGSize(width: size.height, height: size.width)

    UIGraphicsBeginImageContext(rotatedSize)

    if let bitmap = UIGraphicsGetCurrentContext(), let image = self.cgImage {
      bitmap.translateBy(x: rotatedSize.width / 2.0, y: rotatedSize.height / 2.0)

      bitmap.rotate(by: degrees)

      bitmap.scaleBy(x: self.scale, y: -self.scale)

      let rect = CGRect(x: -size.width / 2.0, y: -size.height / 2.0, width: size.width, height: size.height)

      bitmap.draw(image, in: rect)

      let newImage = UIGraphicsGetImageFromCurrentImageContext()

      UIGraphicsEndImageContext()

      return newImage
    }

    return self
  }

  public func invertColors() -> UIImage? {
    let ciImage = CIImage(image: self)

    let context = CIContext(options: nil)

    if let filter = CIFilter(name: "CIPhotoEffectNoir") {
      filter.setValue(ciImage, forKey: kCIInputImageKey)

      if let outputImage = filter.outputImage, let cgImage = context.createCGImage(outputImage, from: outputImage.extent) {
        let newImage = UIImage(cgImage: cgImage)

        return newImage
      }
    }

    return self
  }

}
