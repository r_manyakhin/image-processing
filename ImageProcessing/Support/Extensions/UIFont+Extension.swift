//
//  UIFont+Extension.swift
//  ImageProcessing
//
//  Created by Роман Маняхин on 03.04.2018.
//  Copyright © 2018 Roman Manyakhin. All rights reserved.
//

import UIKit

extension UIFont {

  open class func openSans(_ size: CGFloat) -> UIFont {
    return UIFont(name: "OpenSans", size: size) ?? UIFont.systemFont(ofSize: size)
  }

  open class func openSansBold(_ size: CGFloat) -> UIFont {
    return UIFont(name: "OpenSans-Bold", size: size) ?? UIFont.systemFont(ofSize: size, weight: .bold)
  }

}
